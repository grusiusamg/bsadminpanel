package com.bookStore.service;

import com.bookStore.domain.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {

    Book save(Book book);

    List<Book> findAll();

    Optional<Book> findById(Long id);

    void removeById(Long id);

}
