package com.bookStore.service;

import com.bookStore.domain.User;
import com.bookStore.domain.security.UserRole;

import java.util.Set;

public interface UserService {

    User createUser(User user, Set<UserRole> userRoles)throws Exception;

    User save(User user);
}
