// // disable same as shipping address in checkout
// function checkBillingAddress() {
//     if ($("#theSameAsShippingAddress").is(":checked")) {
//         $(".billingAddress").prop("disabled", true);
//     } else {
//         $(".billingAddress").prop("disabled", false);
//     }
// }
//
// //compare passwords
//
// function checkPasswordMatch() {
//     var password = $("#txtNewPassword").val();
//     var confirmPassword = $("#txtConfirmPassword").val();
//
//     if (password == "" && confirmPassword == "") {
//         $("#checkPasswordMatch").html("");
//         $("#updateUserInfoButton").prop('disabled', false);
//     } else {
//         if (password != confirmPassword) {
//             $("#checkPasswordMatch").html("Passwords do not match!");
//             $("#updateUserInfoButton").prop('disabled', true);
//         } else {
//             $("#checkPasswordMatch").html("Passwords match");
//             $("#updateUserInfoButton").prop('disabled', false);
//         }
//     }
// }
//
// //show update function in shoppingCart.html
// $(document).ready(function () {
//     $(".cartItemQty").on('change', function () {
//         var id = this.id;
//
//         $('#update-item-' + id).css('display', 'inline-block');
//     });
//     $("#theSameAsShippingAddress").on('click', checkBillingAddress);
//     $("#txtConfirmPassword").keyup(checkPasswordMatch);
//     $("#txtNewPassword").keyup(checkPasswordMatch);
// });

//admin panel delete book confirmation
$(document).ready(function() {
    $('.delete-book').on('click', function (){
        /*<![CDATA[*/
        var path = /*[[@{/}]]*/'remove';
        /*]]>*/

        var id=$(this).attr('id');

        bootbox.confirm({
            message: "Are you sure to remove this book? It can't be undone.",
            buttons: {
                cancel: {
                    label:'<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label:'<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(confirmed) {
                if(confirmed) {
                    $.post(path, {'id':id}, function(res) {
                        location.reload();
                    });
                }
            }
        });
    });

    var bookIdList=[];

    $('.checkboxBook').click(function() {
        var id=$(this).attr('id');
        if(this.checked){
            bookIdList.push(id);
        } else {
            bookIdList.splice(bookIdList.indexOf(id), 1);
        }
    })

    $('#deleteSelected').click(function() {
        /*<![CDATA[*/
        var path = /*[[@{/}]]*/'removeList';
        /*]]>*/

        bootbox.confirm({
            message: "Are you sure to remove all selected books? It can't be undone.",
            buttons: {
                cancel: {
                    label:'<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label:'<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(confirmed) {
                if(confirmed) {
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: JSON.stringify(bookIdList),
                        contentType: "application/json",
                        success: function(res) {console.log(res); location.reload()},
                        error: function(res){console.log(res); location.reload();}
                    });
                }
            }
        });
    });

    $("#selectAllBooks").click(function() {
        if($(this).prop("checked")==true) {
            $(".checkboxBook").prop("checked",true);
        } else if ($(this).prop("checked")==false) {
            $(".checkboxBook").prop("checked",false);
        }
    })
});
